
/* login form */
$( "#login" ).dialog({

    autoOpen: false,
    buttons: [
        {
            id: 'enter',
            text: 'Войти',
            click: function() {
                var user_name = $('#name');
                var user_password = $('#password');
                $.ajax({
                    type: "POST",
                    // url: window.location.href + "/users/login/",
                    url: "http://" + ur + "/users/login/",
                    data: {
                        name: user_name.val(),
                        password: user_password.val(),
                    },
                    dataType: 'json',
                    success: function(data){
                        if (data['auth']) {
                            $('#greeting').text('Hello, ' + data['name']);
                            $('#login_btn').hide();
                            $('#logout_btn').show();
                            $('#art_list').show();
                            getArticles();

                            if ( parseInt(data['role']) === 1 ) {
                                getUsers();
                                $('#users_list').show();
                                $('#admin_btn').show();
                            }
                        } else {
                            alert(data['message']);
                        }
                        $('#login').dialog('close');
                    },
                    error: function(data){
                        $('#login').dialog('close');
                    }
                });
            }
        },
        {
            id: 'dont',
            text: 'Отмена',
            click: function() {
                $('#login').dialog('close');
            }
        }
    ],
    show: {
        effect: "drop",
        duration: 500
    },
    hide: {
        effect: "drop",
        duration: 500
    }
});

$('#login_btn').click(function() {

    $( "#login" ).dialog('open');
});

function reset(){
    $.ajax({
        type: "GET",
        // url: window.location.href + "/users/userStatus/",
        url: "http://" + ur + "/users/userStatus/",
        dataType: 'json',
        success: function(data){
            console.log('Check Auth userStatus');                        
            $('#greeting').text('Hello, ' + data['name']);     
            $('#logout_btn').hide();
            $('#login_btn').show();
            $('#art_list').hide();                
            $('#users_list').hide();
            $('#admin_btn').hide();        
        },
        error: function(data){
                console.log('Error');
                $('#logout_alt').hide();
        }
    });
}

$('#logout_btn').click(function() {

    $.ajax({
        type: "GET",
        // url: window.location.href + "/users/logout/",
        url: "http://" + ur + "/users/logout/",
        dataType: 'json',
        success: function(data){
            console.log('user Logout');
            $('div.rem').remove();
            reset();
        },
        error: function(data){
                console.log('Logout Error');
        }
    });
});