<?php

class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {

        $uri = $this->getURI();


        if ($uri == 'public/index.php') {


//            echo "if public/index.php";



            include(ROOT.'/public/index.php');

        } else {



//            echo "else public/index.php";



            foreach ($this->routes as $path) {

                if (preg_match("~$path~", $uri)) {

                    $segments = explode('/', $path);

                    $controllerName = array_shift($segments) . 'Controller';

                    $controllerName = ucfirst($controllerName);

                    $actionName = array_shift($segments);

                    $controllerFile = ROOT . '/controllers/' .
                        $controllerName . '.php';

                    if (file_exists($controllerFile)) {
                        include_once($controllerFile);
                    }

                    $controllerObject = new $controllerName();

                    $result = $controllerObject->$actionName();

                    if ($result != null) {

                        break;
                    }
                }
            }
        }
    }
}