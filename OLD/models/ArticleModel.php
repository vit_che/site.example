<?php


class ArticleModel {


    public function getMysqli(){

        $mysqli = new mysqli('localhost', 'root', 'passwd', 'very_simple_site');

        return $mysqli;
    }

    public function allArticles() {

        $res = $this->getMysqli()->query("SELECT * FROM articles");

        return $res;
    }

    public function getArticle($id) {

        $res = $this->getMysqli()->query("SELECT * FROM articles WHERE id = '$id'");

        return $res;
    }

    public function addArticle($title, $content) {

        $this->getMysqli()->query("INSERT INTO articles (title, content)  VALUES ('$title', '$content') ");

    }

    public function deleteArticle($id) {

        $this->getMysqli()->query("DELETE FROM articles WHERE id = '$id'");

    }

    public function editArticle($id, $title, $content) {

        $this->getMysqli()->query("UPDATE articles
                              SET  title = '$title', content = '$content'
                              WHERE id = '$id'");
    }
}