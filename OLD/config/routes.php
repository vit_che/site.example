<?php

    return array(

       'users/login',
       'users/logout',
       'users/userStatus',
       'users/getUsers',
       'articles/getArticles',
       'articles/getItem',
       'articles/add',
       'articles/delete',
       'articles/edit',
       'login/data'

    );