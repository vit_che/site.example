
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TestSite</title>

    <!-- Bootstrap -->
<!--     <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="/css/jquery-ui.css" rel="stylesheet">
    <link href="/css/jquery.qtip.css" rel="stylesheet" >

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!--<script src="../bootstrap/js/bootstrap.js"></script>-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <style>
        body {
            margin-bottom: 10px;
        }
        .title {
            margin-top: 50px;
            font-family: 'Open Sans', arial;
            font-size: 58px;
            font-weight: 100 ;
            /*    text-shadow: 4px 4px 4px #aaa;*/
            color: grey;
        }
        .pad-top{
            padding-top: 100px;
            background-color: #f4f5f7;
            opacity: 0.9;
            height: 100vh;
            width: 75%;
        }
        .footer{
            font-family: 'Open Sans', arial;
            font-size: 12px;
            font-weight: 100 ;
            position: absolute;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 60px;
        }
        .marg-top-20 {
            margin-top: 20px;
        }

        .backauth {
            /* background-color: #eaecef; */
            background-color: #b3bfd1;
            /* background-image: url("css/images/back-dark.xcf"); */
            /* background-color: #eaecef; */
            /*background-color: #697d9b;*/
        }
        .center {
            text-align: center;
        }
        .adm-btn {
            margin: 0 10px 0 10px;
        }

/*        #login_alt, #logout_alt {
            margin-top: 30px;
        }*/
        .link {
            background-color: #eef1f5;
            font-family: 'Open Sans', arial;
            font-size: 24px;
            font-weight: 100 ;
        }
        a:hover{
            color: rgb(51, 150, 231) !important;
            text-decoration: none;
        }
        .show_event {
            color:black !important;
        }
        .grey {
            background-color: #e3e7ed;
        }
        .ligth {
            background-color: rgb(250, 241, 211);
        }
        .blue {
            background-color: #ccdefc;
        }
    </style>

    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>

    <h6 class="text-center">* * * * *  Тестовое задание  * * * * * *</h6>
    <h6 class="text-center">* * * * *  использование Rest/Ajax/JQuery/PHP * * * * * *</h6>
    <h6 class="text-center">* * * * *  информация обновляется без перезагрузки страницы  * * * * * *</h6>
    <h6 class="text-center">* * * * *  admin видит юзеров / добавляет(редактирует) статьи  * * * * * *</h6>
    <h6 class="text-center">* * * * *  юзер только просматривает статьи  * * * * * *</h6>
    <h6 class="text-center">* * * * *  admin / admin     user / user  * * * * * *</h6>

    <div class="container">
        <h4><div id="greeting" class="text-center"></div></h4>
    </div>

    <div class="container">
    <div class="row">

        <div class="col-2 grey">
            <h6 class="text-center">Service Panel</h6>
            <div class="container text-center login marg-top-20">
                <button id ="login_btn" type="button" class="btn btn-secondary" >Войти</button>
                <button id ="logout_btn" type="button" class="btn btn-secondary" >Выйти</button>
            </div>
        </div>


        <div class="col-6 ligth">
            <h6 class="text-center">Articles List</h6>

            <div id="art_list"></div>


            <!-- A D M I N    B U T T O N   -->
            <div id='admin_btn' class="container" style="display: none;">
                <div class="center">
                    <button id ="add_article_btn" class="ui-button ui-widget ui-corner-all adm-btn">Add article</button>
                    <button id ="list_articles_btn" class="ui-button ui-widget ui-corner-all adm-btn">List articles</button>
                </div>
            </div>
        </div>

        <div class="col-4 grey">
        <h6 class="text-center">Users List</h6>

            <div id="users_list" title="Список пользователей" style="display: none;">
                <table class="table table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">users name</th>
                        <th scope="col">users role</th>
                    </tr>
                    </thead>
                    <tbody id="list_users"></tbody>
                </table>
            </div>

        </div>

    </div>
    </div>

    <!-- LOGIN FORM -->
    <div id="login">
        <div id="enter" title="enter"></div>
        <form>
            <div class="form-group">
                <label for="name">name</label>
                <input type="text" class="form-control" id="name" placeholder="Input name">
            </div>

            <div class="form-group">
                <label for="password">password</label>
                <input type="password" class="form-control" id="password" placeholder="Input password">
            </div>

            <button
                    class="g-recaptcha"
                    data-sitekey="6Lfqx20UAAAAAAiu4eKspAOIo9wdtnkS6BTJAKHm"
                    data-callback="YourOnSubmitFn">
                Submit
            </button>


        </form>
    </div>

    <!-- Creat Article Form -->
    <div id="article_form" title="Article">
        <form>
        <div class="form-group">
            <input id="article_id" type="hidden" name="id" value="">
        </div>

        <div class="form-group">
            <input id="dep_id" type="hidden" name="id" value="">
            <label for="article_title">News title</label>
            <input id="article_title" type="text" class="form-control"  placeholder="Input article title">

            <label for="article_content">News content</label>
            <input id="article_content" type="text" class="form-control"  placeholder="Input article content">
        </div>
        </form>
    </div>
    <!-- List Articles -->
    <div id="list_articles" title="Articles List">
        <table class="table table-striped">
        <thead class="thead-light">
            <tr>
            <th scope="col">Article Titles</th>
            </tr>
        </thead>
        <tbody id="title_tr"></tbody>
        </table>
        <button id ="add_article_btn_list" class="ui-button ui-widget ui-corner-all">Add Article</button>
        <button id ="close_list_btn" class="ui-button ui-widget ui-corner-all">Close List</button>
    </div>


    <script src="http://code.jquery.com/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="/js/jquery-ui.js"></script>
    <script src="/lib/moment.min.js"></script>

    <script src="/js/main.js"></script>
    <script src="/js/login.js"></script>


    <script src="/bootstrap/js/bootstrap.js"></script>


</body>
</html>